# OpenML dataset: hailfinder_dataset

https://www.openml.org/d/43156

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Dataset description **

Hailfinder is a Bayesian network designed to forecast severe summer hail in northeastern Colorado.

**Format of the dataset **

The hailfinder data set contains the following 56 variables:

N07muVerMo (10.7mu vertical motion): a four-level factor with levels StrongUp, WeakUp, Neutral and Down.

SubjVertMo (subjective judgment of vertical motion): a four-level factor with levels StrongUp, WeakUp, Neutral and Down.

QGVertMotion (quasigeostrophic vertical motion): a four-level factor with levels StrongUp, WeakUp, Neutral and Down.

CombVerMo (combined vertical motion): a four-level factor with levels StrongUp, WeakUp, Neutral and Down.

AreaMesoALS (area of meso-alpha): a four-level factor with levels StrongUp, WeakUp, Neutral and Down.

SatContMoist (satellite contribution to moisture): a four-level factor with levels VeryWet, Wet, Neutral and Dry.

RaoContMoist (reading at the forecast center for moisture): a four-level factor with levels VeryWet, Wet, Neutral and Dry.

CombMoisture (combined moisture): a four-level factor with levels VeryWet, Wet, Neutral and Dry.

AreaMoDryAir (area of moisture and adry air): a four-level factor with levels VeryWet, Wet, Neutral and Dry.

VISCloudCov (visible cloud cover): a three-level factor with levels Cloudy, PC and Clear.

IRCloudCover (infrared cloud cover): a three-level factor with levels Cloudy, PC and Clear.

CombClouds (combined cloud cover): a three-level factor with levels Cloudy, PC and Clear.

CldShadeOth (cloud shading, other): a three-level factor with levels Cloudy, PC and Clear.

AMInstabMt (AM instability in the mountains): a three-level factor with levels None, Weak and Strong.

InsInMt (instability in the mountains): a three-level factor with levels None, Weak and Strong.

WndHodograph (wind hodograph): a four-level factor with levels DCVZFavor, StrongWest, Westerly and Other.

OutflowFrMt (outflow from mountains): a three-level factor with levels None, Weak and Strong.

MorningBound (morning boundaries): a three-level factor with levels None, Weak and Strong.

Boundaries (boundaries): a three-level factor with levels None, Weak and Strong.

CldShadeConv (cloud shading, convection): a three-level factor with levels None, Some and Marked.

CompPlFcst (composite plains forecast): a three-level factor with levels IncCapDecIns, LittleChange and DecCapIncIns.

CapChange (capping change): a three-level factor with levels Decreasing, LittleChange and Increasing.

LoLevMoistAd (low-level moisture advection): a four-level factor with levels StrongPos, WeakPos, Neutral and Negative.

InsChange (instability change): three-level factor with levels Decreasing, LittleChange and Increasing.

MountainFcst (mountains (region 1) forecast): a three-level factor with levels XNIL, SIG and SVR.

Date (date): a six-level factor with levels May15_Jun14, Jun15_Jul1, Jul2_Jul15, Jul16_Aug10, Aug11_Aug20 and Aug20_Sep15.

Scenario (scenario): an eleven-level factor with levels A, B, C, D, E, F, G, H, I, J and K.

ScenRelAMCIN (scenario relevant to AM convective inhibition): a two-level factor with levels AB and CThruK.

MorningCIN (morning convective inhibition): a four-level factor with levels None, PartInhibit, Stifling and TotalInhibit.

AMCINInScen (AM convective inhibition in scenario): a three-level factor with levels LessThanAve, Average and MoreThanAve.

CapInScen (capping withing scenario): a three-level factor with levels LessThanAve, Average and MoreThanAve.

ScenRelAMIns (scenario relevant to AM instability): a six-level factor with levels ABI, CDEJ, F, G, H and K.

LIfr12ZDENSd (LI from 12Z DEN sounding): a four-level factor with levels LIGt0, N1GtLIGt_4, N5GtLIGt_8 and LILt_8.

AMDewptCalPl (AM dewpoint calculations, plains): a three-level factor with levels Instability, Neutral and Stability.

AMInsWliScen (AM instability within scenario): a three-level factor with levels LessUnstable, Average and MoreUnstable.

InsSclInScen (instability scaling within scenario): a three-level factor with levels LessUnstable, Average and MoreUnstable.

ScenRel34 (scenario relevant to regions 2/3/4): a five-level factor with levels ACEFK, B, D, GJ and HI.

LatestCIN (latest convective inhibition): a four-level factor with levels None, PartInhibit, Stifling and TotalInhibit.

LLIW (LLIW severe weather index): a four-level factor with levels Unfavorable, Weak, Moderate and Strong.

CurPropConv (current propensity to convection): a four-level factor with levels None, Slight, Moderate and Strong.

ScnRelPlFcst (scenario relevant to plains forecast): an eleven-level factor with levels A, B, C, D, E, F, G, H, I, J and K.

PlainsFcst (plains forecast): a three-level factor with levels XNIL, SIG and SVR.

N34StarFcst (regions 2/3/4 forecast): a three-level factor with levels XNIL, SIG and SVR.

R5Fcst (region 5 forecast): a three-level factor with levels XNIL, SIG and SVR.

Dewpoints (dewpoints): a seven-level factor with levels LowEverywhere, LowAtStation, LowSHighN, LowNHighS, LowMtsHighPl, HighEverywher, Other.

LowLLapse (low-level lapse rate): a four-level factor with levels CloseToDryAd, Steep, ModerateOrLe and Stable.

MeanRH (mean relative humidity): a three-level factor with levels VeryMoist, Average and Dry.

MidLLapse (mid-level lapse rate): a three-level factor with levels CloseToDryAd, Steep and ModerateOrLe.

MvmtFeatures (movement of features): a four-level factor with levels StrongFront, MarkedUpper, OtherRapid and NoMajor.

RHRatio (realtive humidity ratio): a three-level factor with levels MoistMDryL, DryMMoistL and other.

SfcWndShfDis (surface wind shifts and discontinuities): a seven-level factor with levels DenvCyclone, E_W_N, E_W_S, MovigFtorOt, DryLine, None and Other.

SynForcng (synoptic forcing): a five-level factor with levels SigNegative, NegToPos, SigPositive, PosToNeg and LittleChange.

TempDis (temperature discontinuities): a four-level factor with levels QStationary, Moving, None, Other.

WindAloft (wind aloft): a four-level factor with levels LV, SWQuad, NWQuad, AllElse.

WindFieldMt (wind fields, mountains): a two-level factor with levels Westerly and LVorOther.

WindFieldPln (wind fields, plains): a six-level factor with levels LV, DenvCyclone, LongAnticyc, E_NE, SEquad and WidespdDnsl.



**Source **

Abramson B, Brown J, Edwards W, Murphy A, Winkler RL (1996). "Hailfinder: A Bayesian system for forecasting severe weather". International Journal of Forecasting, 12(1):57-71.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43156) of an [OpenML dataset](https://www.openml.org/d/43156). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43156/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43156/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43156/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

